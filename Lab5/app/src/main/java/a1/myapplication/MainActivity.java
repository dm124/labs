package a1.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.ToggleButton;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private View.OnClickListener addButtonOnClickListener;
    private View.OnClickListener deleteButtonOnClickListener;
    private View.OnClickListener sortButtonOnClickListener;
    SimpleCursorAdapter scAdapter;
    ListView lvData;
    private  MyDBHelper mDBHelper;
    private SQLiteDatabase MyDB;
    @SuppressWarnings("unused")
    private String TAG = "TestLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button addButton = (Button) findViewById(R.id.addButton);
        Button deleteButton = (Button) findViewById(R.id.deleteButton);
        Button sortButton = (Button) findViewById(R.id.sortButton);
        final ToggleButton toggleButton = (ToggleButton) findViewById(R.id.toggleButton1);
        final  EditText editText = (EditText) findViewById(R.id.editText2);
        final Spinner profSpinner = (Spinner) findViewById(R.id.spinner2);
        lvData = (ListView) findViewById(R.id.mainListView);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues values = new ContentValues();

                values.put("name", editText.getText().toString());
                if(toggleButton.isChecked()){
                    values.put("sex", 0);
                } else {
                    values.put("sex", 1);
                }

                values.put("prof", profSpinner.getSelectedItem().toString());
                MyDB.insert("PeopleTable", null, values);

                showTable();
            }
        });

       deleteButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String a = scAdapter.getCursor().getString(
                       scAdapter.getCursor().getColumnIndexOrThrow("_id"));
               MyDB.delete("PeopleTable", "_id=" + a, null);

               showTable();
           }
       });

       sortButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               sortTable();
           }
       });

       mDBHelper = new MyDBHelper(this);
       MyDB = mDBHelper.getWritableDatabase();

       showTable();

    }

    public class MyDBHelper extends SQLiteOpenHelper {

        public MyDBHelper(Context context) {
            super(context, "PeopleDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE PeopleTable (_id INTEGER PRIMARY KEY AUTOINCREMENT,"+ " name TEXT NOT NULL, prof TEXT, sex INTEGER)");
            ContentValues values = new ContentValues();

            values.put("name", "Иванов Иван");
            values.put("prof","Плотник");
            values.put("sex","0");
            db.insert("PeopleTable", null, values);

            values.put("name", "Петр Петров");
            values.put("prof","Шахтер");
            values.put("sex","0");
            db.insert("PeopleTable", null, values);

            values.put("name", "Светлана Светлова");
            values.put("prof","Директор");
            values.put("sex","1");
            db.insert("PeopleTable", null, values);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    public void showTable() {
        Cursor c = MyDB
                .query("PeopleTable",null,null,null,null,null,null);

        String[] from = new String[] {"name","sex","prof"};
        int[] to = new int[] {R.id.nameTextView, R.id.sexTextView, R.id.profTextView};

        scAdapter = new SimpleCursorAdapter(this, R.layout.itemlayout, c, from, to);

        lvData.setAdapter(scAdapter);
    }

    public void sortTable() {
        Cursor c = MyDB
                .query("PeopleTable",null,"sex=0",null,null,null,"name ASC");

        String[] from = new String[] {"name","sex","prof"};
        int[] to = new int[] {R.id.nameTextView, R.id.sexTextView, R.id.profTextView};

        scAdapter = new SimpleCursorAdapter(this, R.layout.itemlayout, c, from, to);

        lvData.setAdapter(scAdapter);
    }

}

