package a1.lab3;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ResourceBundle;

public class MainActivity extends AppCompatActivity {
    TextView textViewA;
    TextView textViewB;
    EditText EditTextX;
    EditText EditTextY;
    EditText EditTextZ;
    Button buttonCalc;
    Button buttonClear;
    View.OnClickListener onCalcClick;
    View.OnClickListener onClearClick;

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences appPrefences = getSharedPreferences("MyFile", Context.MODE_PRIVATE);
        SharedPreferences.Editor appEditor = appPrefences.edit();

        appEditor.clear();

        if (EditTextX.getText().length() != 0){
            appEditor.putFloat("EditX", Float.valueOf(EditTextX.getText().toString()));
        }
        if (EditTextY.getText().length() != 0){
            appEditor.putFloat("EditY", Float.valueOf(EditTextY.getText().toString()));
        }
        if (EditTextZ.getText().length() != 0){
            appEditor.putFloat("EditZ", Float.valueOf(EditTextZ.getText().toString()));
        }

        appEditor.putString("textViewA", textViewA.getText().toString());
        appEditor.putString("textViewB", textViewB.getText().toString());

        appEditor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences appPrefences = getSharedPreferences("MyFile", Context.MODE_PRIVATE);

        if (appPrefences.contains("EditX")){
            EditTextX.setText(Float.toString(appPrefences.getFloat("EditX",0)));
        }
        else{
            EditTextX.setText("");
        }
        if (appPrefences.contains("EditX")){
            EditTextY.setText(Float.toString(appPrefences.getFloat("EditY",0)));
        }
        else{
            EditTextY.setText("");
        }
        if (appPrefences.contains("EditX")){
            EditTextZ.setText(Float.toString(appPrefences.getFloat("EditZ",0)));
        }
        else{
            EditTextZ.setText("");
        }

        textViewA.setText(appPrefences.getString("textViewA",getString(R.string.action_tv1)));
        textViewB.setText(appPrefences.getString("textViewB",getString(R.string.action_tv2)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        textViewA = (TextView) findViewById(R.id.textViewA);
        textViewB = (TextView) findViewById(R.id.textViewB);

        EditTextX = (EditText) findViewById(R.id.EditTextX);
        EditTextY = (EditText) findViewById(R.id.EditTextY);
        EditTextZ = (EditText) findViewById(R.id.EditTextZ);

        buttonCalc = (Button) findViewById(R.id.buttonCalc);
        buttonClear = (Button) findViewById(R.id.buttonClear);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        buttonCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float InputX = Float.valueOf(EditTextX.getText().toString());
                float InputY = Float.valueOf(EditTextY.getText().toString());
                float InputZ = Float.valueOf(EditTextZ.getText().toString());

                float a = InputX + InputY + InputZ;
                float b = InputX * InputY * InputZ;

                textViewA.setText(getString(R.string.action_tv1)+ " " + a);
                textViewB.setText(getString(R.string.action_tv2)+ " " + b);
            }
        });

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditTextX.setText("");
                EditTextY.setText("");
                EditTextZ.setText("");

                textViewA.setText(getString(R.string.action_tv1));
                textViewB.setText(getString(R.string.action_tv1));
            }
        });

        TextWatcher editTextChange = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                buttonCalc.setEnabled((EditTextX.getText().length() != 0) &&
                        (EditTextY.getText().length() != 0) &&
                        (EditTextZ.getText().length() != 0));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        EditTextX.addTextChangedListener(editTextChange);
        EditTextY.addTextChangedListener(editTextChange);
        EditTextZ.addTextChangedListener(editTextChange);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id){
            case R.id.action_calc:
                onCalcClick.onClick(null);
                break;
            case  R.id.action_clear:
                onClearClick.onClick(null);
                break;
            case R.id.action_settings:
                SharedPreferences appPrefences = getSharedPreferences("MyFile", Context.MODE_PRIVATE);
                SharedPreferences.Editor appEditor = appPrefences.edit();

                appEditor.clear();
                appEditor.apply();

                onResume();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
