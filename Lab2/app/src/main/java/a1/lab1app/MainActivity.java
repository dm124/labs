package a1.lab1app;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button Button1;
    Button Button2;
    TextView TextView2;
    TextView TextView3;
    TextView TextView4;
    EditText EditText1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button1 = (Button) findViewById(R.id.button1);
        Button2 = (Button) findViewById(R.id.button2);
        EditText1 = (EditText)findViewById(R.id.editText1);
        TextView2 = (TextView)findViewById(R.id.textView2);
        TextView3 = (TextView)findViewById(R.id.textView3);
        TextView4 = (TextView)findViewById(R.id.textView4);

        EditText1.addTextChangedListener(new TextWatcher(){

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Button2.setEnabled(true);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                Button1.setEnabled(EditText1.getText().length() != 0);

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    View.OnClickListener onclicklistener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            EditText1.setText("");
            Button2.setEnabled(false);

        }

    };

    public void onButton1Click(View v){

        if((TextView2.getText().length() > 21) | (TextView3.getText().length() > 21) | (TextView4.getText().length() > 21)  ){
            TextView2.setText(TextView2.getText().toString().substring(0,21));
            TextView3.setText(TextView3.getText().toString().substring(0,21));
            TextView4.setText(TextView4.getText().toString().substring(0,21));
        }

        final  String mytext = EditText1.getText().toString();

        int a = 0;
        int b = 0;
        int c = 0;

        for (int i = 0; i < mytext.length(); i++) {
            switch (mytext.charAt(i)){
                case 'A' :
                case 'a' :
                    a++;
                    break;
                case 'B' :
                case 'b' :
                    b++;
                    break;
                case 'C' :
                case 'c' :
                    c++;
                    break;
            }
        }

        TextView2.setText(TextView2.getText().toString() + ' ' + Integer.toString(a));
        TextView3.setText(TextView3.getText().toString() + ' ' + Integer.toString(b));
        TextView4.setText(TextView4.getText().toString() + ' ' + Integer.toString(c));

    }

    public void onButton2Click(View v){

        Button2.setOnClickListener(onclicklistener2);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if cg
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
